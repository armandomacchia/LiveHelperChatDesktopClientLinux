![alt tag](https://livehelperchat.com/design/frontendnew/images/lhc.png)![alt tag](http://i.imgur.com/QS0zqx8.png)


Desktop client source
=======

Compatible with QT 5.x version.


```sh
$ git clone https://gitlab.com/armando.macchia/LiveHelperChatDesktopClientLinux.git
$ cd cloned rep
$ qmake
$ make
$ sudo make install
```



or build with QtCreator! 


Desktop client Screen running on Archlinux
=======
![alt tag](http://i.imgur.com/1iFIk72.png)